memory = []
ip = 0
rel_addr = 0

def run_program(input: int) -> int:
    global ip, rel_addr, memory
    ip = 0
    rel_addr = 0

    with open('day09_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    # memory = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]
    # memory = [1102,34915192,34915192,7,4,7,99,0]
    # memory = [104,1125899906842624,99]
    # extend memory
    memory += [0]*10000

    def read(addr, mode):
        if mode == '0': # ordinary
            return memory[addr]
        if mode == '1': # immediate
            return addr
        if mode == '2': # relative
            return memory[addr + rel_addr]
        print('wtf')
    
    def write(addr, value, mode='0'):
        if mode == '0': # ordinary
            memory[addr] = value
        # if mode == '1': # immediate not valid
        #     return addr
        elif mode == '2': # relative
            memory[addr + rel_addr] = value
        else:
            print('wtf')


    def op_add(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 + op2, m_d)
        return ip + 4

    def op_mul(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 * op2, m_d)
        return ip + 4

    def op_input(dst_addr, m_d, input):
        write(dst_addr, input, m_d)
        return ip + 2

    def op_output(src_addr, im, ip):
        op1 = read(src_addr, im)
        print(op1)
        return ip + 2

    def op_jt(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 == 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_jf(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 != 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_lt(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 < op2 else 0, m_3)
        return ip + 4

    def op_eq(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 == op2 else 0, m_3)
        return ip + 4

    def op_adjr(src_1, m_1):
        global rel_addr
        op1 = read(src_1, m_1)
        rel_addr += op1
        return ip + 2

    OFFSET_OPCODE = 4
    OFFSET_M_1 = 2
    OFFSET_M_2 = 1
    OFFSET_M_3 = 0

    while True:
        instr = str(memory[ip])
        if instr == '99':
            break

        instr = instr.zfill(5)
        #print(instr)

        if instr[OFFSET_OPCODE] == '1': # add
            ip = op_add(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '2': # mul
            ip = op_mul(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '3': # store input
            ip = op_input(memory[ip+1], instr[OFFSET_M_1], input)
        elif instr[OFFSET_OPCODE] == '4': # output
            ip = op_output(memory[ip+1], instr[OFFSET_M_1], ip)
        elif instr[OFFSET_OPCODE] == '5': # jt
            ip = op_jt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '6': # jf
            ip = op_jf(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '7': # lt
            ip = op_lt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '8': # eq
            ip = op_eq(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '9': # relative addr
            ip = op_adjr(memory[ip+1], instr[OFFSET_M_1])
        else:
            print('wtf')



def main():
    run_program(1)
    run_program(2)
    
if __name__ == '__main__':
    main()