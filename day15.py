from textwrap import wrap
import random

memory = []
ip = 0
rel_addr = 0

def run_program_to_output(input: int):
    global ip, rel_addr, memory

    def read(addr, mode):
        if mode == '0': # ordinary
            return memory[addr]
        if mode == '1': # immediate
            return addr
        if mode == '2': # relative
            return memory[addr + rel_addr]
        print('wtf')
    
    def write(addr, value, mode='0'):
        if mode == '0': # ordinary
            memory[addr] = value
        elif mode == '2': # relative
            memory[addr + rel_addr] = value
        else:
            print('wtf')


    def op_add(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 + op2, m_d)
        return ip + 4

    def op_mul(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 * op2, m_d)
        return ip + 4

    def op_input(dst_addr, m_d, input):
        write(dst_addr, input, m_d)
        return ip + 2

    def op_output(src_addr, im):
        op1 = read(src_addr, im)
        #print(op1)
        return ip + 2, op1

    def op_jt(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 == 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_jf(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 != 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_lt(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 < op2 else 0, m_3)
        return ip + 4

    def op_eq(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 == op2 else 0, m_3)
        return ip + 4

    def op_adjr(src_1, m_1):
        global rel_addr
        op1 = read(src_1, m_1)
        rel_addr += op1
        return ip + 2

    OFFSET_OPCODE = 4
    OFFSET_M_1 = 2
    OFFSET_M_2 = 1
    OFFSET_M_3 = 0

    while True:
        instr = str(memory[ip])
        if instr == '99':
            return -1

        instr = instr.zfill(5)
        #print(instr)

        if instr[OFFSET_OPCODE] == '1': # add
            ip = op_add(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '2': # mul
            ip = op_mul(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '3': # store input
            ip = op_input(memory[ip+1], instr[OFFSET_M_1], input)
        elif instr[OFFSET_OPCODE] == '4': # output
            ip, output = op_output(memory[ip+1], instr[OFFSET_M_1])
            return output
        elif instr[OFFSET_OPCODE] == '5': # jt
            ip = op_jt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '6': # jf
            ip = op_jf(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '7': # lt
            ip = op_lt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '8': # eq
            ip = op_eq(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '9': # relative addr
            ip = op_adjr(memory[ip+1], instr[OFFSET_M_1])
        else:
            print('wtf')

def build_map():

    screen = {}
    global memory, ip, rel_addr
    ip = 0
    rel_addr = 0

    with open('day15_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    # extend memory
    memory += [0]*10000

    droid_pos = [21,19]
    oxygen_pos = None
    prev_dir = None
    offsets = {}
    offsets[1] = [0,1]
    offsets[2] = [0,-1]
    offsets[3] = [-1,0]
    offsets[4] = [1,0]

    image = ['*'] * 41 * 41
    image[droid_pos[1] * 41 + droid_pos[0]] = 'D'

    def get_next_pos(pos, instr):
        next_pos = list(pos)
        next_pos[0] += offsets[instr][0]
        next_pos[1] += offsets[instr][1]
        return next_pos


    def choose_dir(pos, screen, prev_dir):
        candidates = range(1, 5)
                
        def f(instr):
            new_pos = get_next_pos(pos, instr)
            if tuple(new_pos) in screen and screen[tuple(new_pos)] == '#': # blocked
                return False
            return True
        
        candidates = list(filter(f, candidates))

        # if we have more then 1 candidate we are not allowed to do a 180
        if len(candidates) > 1 and prev_dir:
            disallowed = {1:2, 2:1, 3:4, 4:3}
            if disallowed[prev_dir] in candidates:
                candidates.remove(disallowed[prev_dir])
        
        #print(candidates)
        return random.sample(candidates,1)[0]

    found = 0
    while found < 15:
        #instr = random.sample(range(1,5), 1)[0]
        instr = choose_dir(droid_pos, screen, prev_dir)

        outp = run_program_to_output(instr)
        if outp == 0:
            wall_pos = list(droid_pos)
            wall_pos[0] += offsets[instr][0]
            wall_pos[1] += offsets[instr][1]
            screen[tuple(wall_pos)] = '#'
            image[wall_pos[1] * 41 + wall_pos[0]] = '#'
        elif outp == 1:
            prev_dir = instr
            droid_pos[0] += offsets[instr][0]
            droid_pos[1] += offsets[instr][1]
            screen[tuple(droid_pos)] = '.'
            if image[droid_pos[1] * 41 + droid_pos[0]] != 'D':
                image[droid_pos[1] * 41 + droid_pos[0]] = '.'
        else:
            prev_dir = instr
            droid_pos[0] += offsets[instr][0]
            droid_pos[1] += offsets[instr][1]
            print(f'Found Oxygen {droid_pos}')
            screen[tuple(droid_pos)] = 'O'
            oxygen_pos = list(droid_pos)
            image[droid_pos[1] * 41 + droid_pos[0]] = 'O'
            found += 1

    i_image = wrap(''.join([str(c) for c in image]), 41)
    for l in i_image:
        print(l)

    print('x')
    print(min(screen.keys(),key=lambda x: x[0]))
    print(max(screen.keys(),key=lambda x: x[0]))

    print('y')
    print(min(screen.keys(),key=lambda x: x[1]))
    print(max(screen.keys(),key=lambda x: x[1]))

def bfs_shortest_path(graph, start, goal):
    explored = []
    queue = [[start]]

    while queue:
        path = queue.pop(0)
        node = path[-1]
        if node not in explored:
            curr_x, curr_y = node
            neighbours = []
            if graph[curr_y * 41 + curr_x + 1] != '#':
                neighbours.append((curr_x + 1, curr_y))
            if graph[curr_y * 41 + curr_x - 1] != '#':
                neighbours.append((curr_x - 1, curr_y))
            if graph[(curr_y + 1) * 41 + curr_x] != '#':
                neighbours.append((curr_x, curr_y + 1))
            if graph[(curr_y - 1) * 41 + curr_x] != '#':
                neighbours.append((curr_x, curr_y - 1))

            for neighbour in neighbours:
                new_path = list(path)
                new_path.append(neighbour)
                queue.append(new_path)
                # return path if neighbour is goal
                if neighbour == goal:
                    return new_path

        # mark node as explored
        explored.append(node)

def main_a(image, droid_pos: tuple, oxygen_pos: tuple):
    p = bfs_shortest_path(image, droid_pos, oxygen_pos)
    #print(p)
    print(f'A : {len(p) - 1}')

depth = 0

def main_b(image, oxygen_pos):
    
    def flood_fill(data, pos, iter) -> int:

        # i_image = wrap(''.join([str(c) for c in data]), 41)
        # for l in i_image:
        #     print(l)

        curr_x, curr_y = pos

        if data[curr_y * 41 + curr_x] == '.':
            data[curr_y * 41 + curr_x] = 'O'

            global depth
            depth = max(depth, iter)

            flood_fill(data, (curr_x + 1, curr_y), iter + 1)
            flood_fill(data, (curr_x - 1, curr_y), iter + 1)
            flood_fill(data, (curr_x, curr_y + 1), iter + 1)
            flood_fill(data, (curr_x, curr_y - 1), iter + 1)


    flood_fill(image, oxygen_pos, 0)
    print(f'B : {depth}')

    # i_image = wrap(''.join([str(c) for c in image]), 41)
    # for l in i_image:
    #     print(l)



if __name__ == '__main__':
    #build_map()

    with open('day15_map.txt', 'r') as f:
        data = list(f.read().replace('\n', '').replace('\r', '').replace('*', '#').replace('D', '.').replace('O', '.'))
    
    main_a(data, (21,19), (33,5))
    main_b(data, (33,5))
