def trace_path(pos, dir, output):
    offset = (0, 0)
    if dir[0] == 'R':
        offset = (1, 0)
    elif dir[0] == 'L':
        offset = (-1, 0)
    elif dir[0] == 'U':
        offset = (0, 1)
    elif dir[0] == 'D':
        offset = (0, -1)

    for _ in range(dir[1]):
        pos[0] += offset[0]
        pos[1] += offset[1]
        output.append(list(pos))

def manhattan(a, b) -> int:
    return abs(a[0]- b[0]) + abs(a[1]- b[1])

def main():
    with open('day03_input.txt', 'r') as f:
        wire_one = list(map(lambda x: (x[0], int(x[1:])), f.readline().split(',')))
        wire_two = list(map(lambda x: (x[0], int(x[1:])), f.readline().split(',')))

    o = [1,1]

    path_one = [list(o)]
    for op in wire_one:
        trace_path(list(path_one[-1]), op, path_one)

    path_two = [list(o)]
    for op in wire_two:
        trace_path(list(path_two[-1]), op, path_two)

    crossings = list(set(map(tuple, path_one)) & set(map(tuple, path_two)))

    dists = []
    for c in crossings:
        dists.append(manhattan(o, c))

    print(f'A : {list(sorted(dists))[1]}')

    dists = []
    for c in crossings:
        l1 = path_one.index(list(c))
        l2 = path_two.index(list(c))
        dists.append(l1 + l2)

    print(f'B : {list(sorted(dists))[1]}')


if __name__ == '__main__':
    main()
