def run_program(input: int) -> int:
    memory = []
    ip = 0

    with open('day05_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    def op_add(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = op1 + op2
        return ip + 4

    def op_mul(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = op1 * op2
        return ip + 4

    def op_input(dst_addr, input, ip):
        memory[dst_addr] = input
        return ip + 2

    def op_output(src_addr, im, ip):
        op1 = src_addr if im else memory[src_addr]
        print(op1)
        return ip + 2

    def op_jt(src_1, src_2, im1, im2, ip):
        op1 = src_1 if im1 else memory[src_1]
        if op1 == 0:
            return ip + 3

        op2 = src_2 if im2 else memory[src_2]
        return op2

    def op_jf(src_1, src_2, im1, im2, ip):
        op1 = src_1 if im1 else memory[src_1]
        if op1 != 0:
            return ip + 3

        op2 = src_2 if im2 else memory[src_2]
        return op2        

    def op_lt(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = 1 if op1 < op2 else 0
        return ip + 4

    def op_eq(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = 1 if op1 == op2 else 0
        return ip + 4

    while True:
        instr = str(memory[ip])
        if instr == '99':
            break

        instr = instr.zfill(4)

        if instr[3] == '1': # add
            ip = op_add(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        elif instr[3] == '2': # mul
            ip = op_mul(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        elif instr[3] == '3': # store input
            ip = op_input(memory[ip+1], input, ip)
        elif instr[3] == '4': # output
            ip = op_output(memory[ip+1], instr[1] == '1', ip)
        elif instr[3] == '5': # jt
            ip = op_jt(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', ip)
        elif instr[3] == '6': # jf
            ip = op_jf(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', ip)
        elif instr[3] == '7': # lt
            ip = op_lt(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        elif instr[3] == '8': # eq
            ip = op_eq(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        else:
            print('wtf')



def main():
    run_program(1)
    run_program(5)
    
if __name__ == '__main__':
    main()