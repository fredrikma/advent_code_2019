from textwrap import wrap

memory = []
ip = 0
rel_addr = 0

def run_program_to_output(input: int):
    global ip, rel_addr, memory

    def read(addr, mode):
        if mode == '0': # ordinary
            return memory[addr]
        if mode == '1': # immediate
            return addr
        if mode == '2': # relative
            return memory[addr + rel_addr]
        print('wtf')
    
    def write(addr, value, mode='0'):
        if mode == '0': # ordinary
            memory[addr] = value
        elif mode == '2': # relative
            memory[addr + rel_addr] = value
        else:
            print('wtf')


    def op_add(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 + op2, m_d)
        return ip + 4

    def op_mul(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 * op2, m_d)
        return ip + 4

    def op_input(dst_addr, m_d, input):
        write(dst_addr, input, m_d)
        return ip + 2

    def op_output(src_addr, im):
        op1 = read(src_addr, im)
        #print(op1)
        return ip + 2, op1

    def op_jt(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 == 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_jf(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 != 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_lt(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 < op2 else 0, m_3)
        return ip + 4

    def op_eq(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 == op2 else 0, m_3)
        return ip + 4

    def op_adjr(src_1, m_1):
        global rel_addr
        op1 = read(src_1, m_1)
        rel_addr += op1
        return ip + 2

    OFFSET_OPCODE = 4
    OFFSET_M_1 = 2
    OFFSET_M_2 = 1
    OFFSET_M_3 = 0

    while True:
        instr = str(memory[ip])
        if instr == '99':
            return -1

        instr = instr.zfill(5)
        #print(instr)

        if instr[OFFSET_OPCODE] == '1': # add
            ip = op_add(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '2': # mul
            ip = op_mul(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '3': # store input
            ip = op_input(memory[ip+1], instr[OFFSET_M_1], input)
        elif instr[OFFSET_OPCODE] == '4': # output
            ip, output = op_output(memory[ip+1], instr[OFFSET_M_1])
            return output
        elif instr[OFFSET_OPCODE] == '5': # jt
            ip = op_jt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '6': # jf
            ip = op_jf(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '7': # lt
            ip = op_lt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '8': # eq
            ip = op_eq(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '9': # relative addr
            ip = op_adjr(memory[ip+1], instr[OFFSET_M_1])
        else:
            print('wtf')

def main_a():

    screen = {}
    global memory, ip, rel_addr
    ip = 0
    rel_addr = 0

    with open('day13_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    # extend memory
    memory += [0]*10000

    while True:
        x, y, tile = run_program_to_output(None), run_program_to_output(None), run_program_to_output(None)
        if x == -1:
            break
        screen[(x, y)] = tile

    print(f'A : {list(screen.values()).count(2)}')

    print(max(screen.keys(),key=lambda x: x[0]))
    print(max(screen.keys(),key=lambda x: x[1]))

def main_b():

    screen = {}
    global memory, ip, rel_addr
    ip = 0
    rel_addr = 0

    with open('day13_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    # extend memory
    memory += [0]*10000
    memory[0] = 2

    #
    # 44x20
    screen = [0] * 44 * 20
    score = 0
    joystick = 0

    paddle_pos = None
    ball_pos = None
    joystick = 0

    while True:
        x, y, tile = run_program_to_output(joystick), run_program_to_output(joystick), run_program_to_output(joystick)
        if x == -1 and y == -1:
            break
        if x == -1:
            score = tile
        else:
            screen[y * 44 + x] = tile if tile != 0 else ' '
            if tile == 3:
                paddle_pos = x
            if tile == 4:
                ball_pos = x
            
            if paddle_pos and ball_pos and paddle_pos < ball_pos:
                joystick = 1
            elif paddle_pos and ball_pos and paddle_pos > ball_pos:
                joystick = -1
            elif paddle_pos and ball_pos and paddle_pos == ball_pos:
                joystick = 0

    print(f'B : {score}')
    image = wrap(''.join([str(c) for c in screen]), 44)
    for l in image:
        print(l)


if __name__ == '__main__':
    main_a()
    main_b()