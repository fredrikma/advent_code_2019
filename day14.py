import math

reactions = {}
reagents = {}
ore_mappings = {}

def find_ore(reagent, amount):
    #print(amount, reagent)

    # already available?
    if reagents[reagent] >= amount:
        #print(f'Used {amount} {reagent} from {reagents[reagent]}')
        reagents[reagent] = reagents[reagent] - amount
        return

    # use the ones we have already
    n_already = reagents[reagent]
    amount -= n_already
    reagents[reagent] = 0

    # last level so translate to ORE
    if reagent in ore_mappings:
        ore_m = ore_mappings[reagent]

        num_ore = ore_m[0]
        num_rea = ore_m[1]

        n = math.ceil(amount / num_rea)
        #print(f'Used {n * num_ore} ORE')
        reagents['ORE'] = reagents['ORE'] + n * num_ore

        reagents[reagent] = reagents[reagent] + num_rea * n - amount
        return

    # find recipe
    recipe = reactions[reagent][1]

    # how many does the recipe produce
    n_per_recipe = reactions[reagent][0]

    # how many iterations of the recipe do we need
    n_to_run = math.ceil(amount  / n_per_recipe)
    #print(f'{n_per_recipe} {amount} {n_to_run}')
    
    # produce each ingredient
    for ingredient in recipe:
        find_ore(ingredient[1], ingredient[0] * n_to_run)

    reagents[reagent] = reagents[reagent] + n_per_recipe * n_to_run - amount

def main_a():
    with open('day14_input.txt', 'r') as f:
        lines = f.read().splitlines()

    for r in lines:
        inp, outp = r.split('=>')
        inps = inp.split(',')

        outp = outp.strip()
        inps = list(map(lambda x: x.strip(), inps))
        
        inps = list(map(lambda x: [int(x.split(' ')[0]), x.split(' ')[1]], inps))
        outp = (int(outp.split(' ')[0]), outp.split(' ')[1])

        assert(outp[1] not in reactions)
        reactions[outp[1]] = (outp[0], inps)

        reagents[outp[1]] = 0
        if len(inps) == 1 and inps[0][1] == 'ORE':
            ore_mappings[outp[1]] = (inps[0][0], outp[0])

    reagents['ORE'] = 0
    find_ore('FUEL', 1)
    print(f'A : {reagents["ORE"]}')

  
def main_b():
    with open('day14_input.txt', 'r') as f:
        lines = f.read().splitlines()

    reactions = {}

    for r in lines:
        inp, outp = r.split('=>')
        inps = inp.split(',')

        outp = outp.strip()
        inps = list(map(lambda x: x.strip(), inps))
        
        inps = list(map(lambda x: [int(x.split(' ')[0]), x.split(' ')[1]], inps))
        outp = (int(outp.split(' ')[0]), outp.split(' ')[1])

        assert(outp[1] not in reactions)
        reactions[outp[1]] = (outp[0], inps)

        reagents[outp[1]] = 0
        if len(inps) == 1 and inps[0][1] == 'ORE':
            ore_mappings[outp[1]] = (inps[0][0], outp[0])

    reagents['ORE'] = 0

    limits = [1, 10000000000]
    fuel_amount = -1
    target = 1000000000000

    while True:
        for k in reagents.keys():
            reagents[k] = 0

        new_amount = limits[0] + (limits[1] - limits[0]) // 2
        if new_amount == fuel_amount:
            break
        fuel_amount = new_amount

        find_ore('FUEL', fuel_amount)
        amount = reagents['ORE']
        if amount < target:
            limits[0] = fuel_amount
        elif amount > target:
            limits[1] = fuel_amount


    print(f'B : {fuel_amount}')
    


if __name__ == '__main__':
    main_a()
    main_b()