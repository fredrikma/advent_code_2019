def main():
    def validate_a(p: int) -> int:
        pwd = str(p)
        b_adj = False
        b_inc = True
        for i in range(len(pwd) - 1):
            if pwd[i] == pwd[i+1]:
                b_adj = True
            if pwd[i] > pwd[i+1]:
                b_inc = False
                break

        if b_adj and b_inc:
            return 1
        return 0

    s = 0
    for p in range(347312, 805915+1):
        s += validate_a(p)
    print(f'A: {s}')

    def check_adj(p: int) -> int:
        pwd = str(p)
        for d in range(1,10):
            s = ''.join(list(map(lambda x: '1' if int(x) == d else '-', pwd)))
            if '11' in s and not '111' in s:
                return True
        return False

    def validate_b(p: int) -> int:
        pwd = str(p)
        for i in range(len(pwd) - 1):
            if pwd[i] > pwd[i+1]:
                return 0

        if not check_adj(p):
            return 0

        return 1

    s = 0
    for p in range(347312, 805915+1):
        s += validate_b(p)
    print(f'B: {s}')


if __name__ == '__main__':
    main()
