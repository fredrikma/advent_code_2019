

whole_map = {}
key_positions = {}
door_positions = {}

with open ('day18_input_ex1.txt', 'r') as f:
    data = f.read().splitlines()
    for y, l in enumerate(data):
        for x, c in enumerate(l):
            whole_map[(x, y)] = c

            if ord(c) >= ord('a') and ord(c) <= ord('z'):
                key_positions[c] = (x,y)
            if ord(c) >= ord('A') and ord(c) <= ord('Z'):
                door_positions[c] = (x,y)

print(whole_map)
print(list(whole_map.keys())[list(whole_map.values()).index('@')])

print(key_positions)
print(door_positions)

# def flood_fill(big_map, pos, small_map) -> int:

#     # i_image = wrap(''.join([str(c) for c in data]), 41)
#     # for l in i_image:
#     #     print(l)

#     curr_x, curr_y = pos

#     if big_map[curr_y * 41 + curr_x] == '.':
#         data[curr_y * 41 + curr_x] = 'O'

#         global depth
#         depth = max(depth, iter)

#         flood_fill(data, (curr_x + 1, curr_y), iter + 1)
#         flood_fill(data, (curr_x - 1, curr_y), iter + 1)
#         flood_fill(data, (curr_x, curr_y + 1), iter + 1)
#         flood_fill(data, (curr_x, curr_y - 1), iter + 1)