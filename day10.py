from math import gcd, sqrt, atan2

def get_dir(coord: tuple) -> tuple:
    c = gcd(coord[0], coord[1])
    return (coord[0] // c, coord[1] // c), c

def is_blocked(origin: tuple, target: tuple, memory: dict) -> bool:
    dir, n = get_dir((target[0] - origin[0], target[1] - origin[1]))

    trace = []
    curr = origin
    for _ in range(n-1):
        curr = (curr[0] + dir[0], curr[1] + dir[1])
        assert(curr in memory)

        if memory[curr]:
            return True
  
    return False


def main_a():
    with open('day10_input.txt', 'r') as f:
        memory_map = list(map(lambda x: False if x == '.' else True, list(f.read().replace('\n', ''))))

    memory = {}
    size = int(sqrt(len(memory_map)))
    for y in range(size):
        for x in range(size):
            memory[(x, y)] = memory_map[y * size + x]

    visible = {}
    for origin, v in memory.items():
        if not v:
            continue
        
        visible[origin] = []

        for target, s in memory.items():
            if not s or origin == target:
                continue

            if not is_blocked(origin, target, memory):
                visible[origin].append(target)

    print(f'A: {max(map(lambda x: len(x), visible.values()))}')

def calc_angle(origin, target):
    up = (0, -1)
    dir = (target[0] - origin[0], target[1] - origin[1])

    dot = up[0] * dir[0] + up[1] * dir[1]
    det = up[0] * dir[1] - up[1] * dir[0]
    angle = atan2(det, dot)
    angle *= 180.0 / 3.1415927
    if angle < 0:
        angle += 360
    return angle

def main_b():
    with open('day10_input.txt', 'r') as f:
        memory_map = list(map(lambda x: False if x == '.' else True, list(f.read().replace('\n', ''))))

    memory = {}
    size = int(sqrt(len(memory_map)))
    for y in range(size):
        for x in range(size):
            memory[(x, y)] = memory_map[y * size + x]

    visible = {}
    for origin, v in memory.items():
        if not v:
            continue
        
        visible[origin] = []

        for target, s in memory.items():
            if not s or origin == target:
                continue

            if not is_blocked(origin, target, memory):
                visible[origin].append(target)

    best = None
    num_visible = 0
    for k, v in visible.items():
        if len(v) > num_visible:
            num_visible = len(v)
            best = k
    
    print(best)
    print(num_visible)

    visible_from_best = visible[best]

    # order by angle
    nuked = sorted(map(lambda x: (calc_angle(best, x), x), visible_from_best), key=lambda x: x[0])[199]
    print(f'B: {nuked[1][0] * 100 + nuked[1][1]}')

if __name__ == '__main__':
    main_a()
    main_b()