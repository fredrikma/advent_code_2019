import re
from itertools import combinations
from operator import add
import math

def main_a():
    with open('day12_input.txt', 'r') as f:
        moons = f.read().splitlines()

    for i, m in enumerate(moons):
        x = int(re.search('x=[+-]?\d+', m).group(0)[2:])
        y = int(re.search('y=[+-]?\d+', m).group(0)[2:])
        z = int(re.search('z=[+-]?\d+', m).group(0)[2:])

        moons[i] = [[x,y,z], [0,0,0]]

    for _ in range(1000):
        # gravity
        for m_1, m_2 in combinations(moons, 2):
            for ax in range(3):
                v_1 = v_2 = 0
                if m_1[0][ax] < m_2[0][ax]:
                    v_1 = 1
                    v_2 = -1
                elif m_1[0][ax] > m_2[0][ax]:
                    v_1 = -1
                    v_2 = 1

                m_1[1][ax] += v_1
                m_2[1][ax] += v_2

        # velocity
        for m in moons:
            m[0] = list(map(add, m[0], m[1]))

    tot = 0
    for m in moons:
        pe = sum(map(abs, m[0]))
        pk = sum(map(abs, m[1]))
        tot += pe * pk
    
    print(f'A : {tot}')

def lcm(a, b):
    return a * b // math.gcd(a, b)

def main_b():
    with open('day12_input.txt', 'r') as f:
        moons = f.read().splitlines()

    for i, m in enumerate(moons):
        x = int(re.search('x=[+-]?\d+', m).group(0)[2:])
        y = int(re.search('y=[+-]?\d+', m).group(0)[2:])
        z = int(re.search('z=[+-]?\d+', m).group(0)[2:])

        moons[i] = [[x,y,z], [0,0,0]]

    initial_state = [[m[0][ax] for m in moons] for ax in range(3)]
    axis_cycle = [0] * 3

    t = 1
    while not all(axis_cycle):
        # gravity
        for m_1, m_2 in combinations(moons, 2):
            for ax in range(3):
                v_1 = v_2 = 0
                if m_1[0][ax] < m_2[0][ax]:
                    v_1 = 1
                    v_2 = -1
                elif m_1[0][ax] > m_2[0][ax]:
                    v_1 = -1
                    v_2 = 1

                m_1[1][ax] += v_1
                m_2[1][ax] += v_2

        # velocity
        for i, m in enumerate(moons):
            m[0] = list(map(add, m[0], m[1]))

        t += 1

        # cycle detection
        current_state = [[m[0][ax] for m in moons] for ax in range(3)]

        for ax in range(3):
            if not axis_cycle[ax] and initial_state[ax] == current_state[ax]:
                axis_cycle[ax] = t

    print(axis_cycle)
    print(f'B : {lcm(lcm(axis_cycle[0], axis_cycle[1]), axis_cycle[2])}')

if __name__ == '__main__':
    main_a()
    main_b()
