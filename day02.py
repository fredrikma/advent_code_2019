from functools import reduce

def run_program(noun: int, verb: int) -> int:
    memory = []
    with open('day02_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    memory[1] = noun
    memory[2] = verb

    pos = 0
    while True:
        opcode = memory[pos]
        if opcode == 99:
            break

        op1 = memory[memory[pos+1]]
        op2 = memory[memory[pos+2]]
        addr = memory[pos+3]
        pos += 4

        if opcode == 1:
            memory[addr] = op1 + op2
        elif opcode == 2:
            memory[addr] = op1 * op2
        else:
            print('wtf')

    return memory[0]


def main():
    print(f'A: {run_program(12, 2)}')

    for n in range(100):
        for v in range(100):
            if run_program(n, v) == 19690720:
                print(f'B: {n}, {v} => {100 * n + v}')
                return

    print('wtf')

if __name__ == '__main__':
    main()
