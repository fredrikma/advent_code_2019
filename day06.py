from functools import reduce

def main_a():
    with open('day06_input.txt', 'r') as f:
        src_orbits = f.read().splitlines()

    orbits = {}
    def build(p):
        a, b = p.split(')')
        orbits[b] = a

    for p in src_orbits:
       build(p)

    def level(k, curr=0):
        if k == 'COM':
            return curr
        return level(orbits[k], curr + 1)

    print(sum(map(level, orbits.keys())))

def main_b():
    with open('day06_input.txt', 'r') as f:
        src_orbits = f.read().splitlines()

    orbits = {}
    def build(p):
        a, b = p.split(')')
        orbits[b] = a

    for p in src_orbits:
       build(p)

    def build_route(k, r):
        if k == 'COM':
            return r
        
        r.append(k)
        return build_route(orbits[k], r)

    r_you = build_route('YOU', [])
    r_san = build_route('SAN', [])

    for s in r_san:
        if s in r_you:
            print(s)
            print(r_san.index(s) + r_you.index(s) - 2)
            break

if __name__ == '__main__':
    main_b()