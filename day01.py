from functools import reduce


def main():
    with open('day01_input.txt', 'r') as f:
        data = f.read().splitlines()

    r = reduce(lambda x, y: x + y, map(lambda x: int(x) // 3 - 2, data))
    print(f'1: Fuel required {r}')

    def fuel_fuel(x: int, tot: int):
        x = x // 3 - 2
        if x > 0:
            return fuel_fuel(x, tot + x)
        return tot

    r = reduce(lambda x, y: x + y, map(lambda x: fuel_fuel(int(x), 0), data))
    print(f'2: Fuel required {r}')


if __name__ == '__main__':
    main()
