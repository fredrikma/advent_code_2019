from itertools import permutations

def run_program(inputs) -> int:
    memory = []
    ip = 0

    with open('day07_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    def op_add(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = op1 + op2
        return ip + 4

    def op_mul(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = op1 * op2
        return ip + 4

    def op_input(dst_addr, input, ip):
        memory[dst_addr] = input
        return ip + 2

    def op_output(src_addr, im, ip):
        op1 = src_addr if im else memory[src_addr]
        #print(op1)
        return ip + 2, op1

    def op_jt(src_1, src_2, im1, im2, ip):
        op1 = src_1 if im1 else memory[src_1]
        if op1 == 0:
            return ip + 3

        op2 = src_2 if im2 else memory[src_2]
        return op2

    def op_jf(src_1, src_2, im1, im2, ip):
        op1 = src_1 if im1 else memory[src_1]
        if op1 != 0:
            return ip + 3

        op2 = src_2 if im2 else memory[src_2]
        return op2        

    def op_lt(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = 1 if op1 < op2 else 0
        return ip + 4

    def op_eq(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = 1 if op1 == op2 else 0
        return ip + 4

    last_output = -1
    while True:
        instr = str(memory[ip])
        if instr == '99':
            return last_output

        instr = instr.zfill(4)

        if instr[3] == '1': # add
            ip = op_add(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        elif instr[3] == '2': # mul
            ip = op_mul(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        elif instr[3] == '3': # store input
            ip = op_input(memory[ip+1], inputs.pop(0), ip)
        elif instr[3] == '4': # output
            ip, last_output = op_output(memory[ip+1], instr[1] == '1', ip)
        elif instr[3] == '5': # jt
            ip = op_jt(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', ip)
        elif instr[3] == '6': # jf
            ip = op_jf(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', ip)
        elif instr[3] == '7': # lt
            ip = op_lt(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        elif instr[3] == '8': # eq
            ip = op_eq(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
        else:
            print('wtf')



def main_a():
    r = 0
    for phase in permutations(range(5)):
        inp = 0
        inp = run_program([phase[0], inp])
        inp = run_program([phase[1], inp])
        inp = run_program([phase[2], inp])
        inp = run_program([phase[3], inp])
        inp = run_program([phase[4], inp])

        r = max(r, inp)
        
    print(f'A: {r}')

def step_program(id, memory, ip, inputs, outputs):

    def op_add(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = op1 + op2
        return ip + 4

    def op_mul(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = op1 * op2
        return ip + 4

    def op_input(dst_addr, input, ip):
        memory[dst_addr] = input
        return ip + 2

    def op_output(src_addr, im, ip):
        op1 = src_addr if im else memory[src_addr]
        #print(op1)
        return ip + 2, op1

    def op_jt(src_1, src_2, im1, im2, ip):
        op1 = src_1 if im1 else memory[src_1]
        if op1 == 0:
            return ip + 3

        op2 = src_2 if im2 else memory[src_2]
        return op2

    def op_jf(src_1, src_2, im1, im2, ip):
        op1 = src_1 if im1 else memory[src_1]
        if op1 != 0:
            return ip + 3

        op2 = src_2 if im2 else memory[src_2]
        return op2        

    def op_lt(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = 1 if op1 < op2 else 0
        return ip + 4

    def op_eq(src_1, src_2, im1, im2, dst_addr, ip):
        op1 = src_1 if im1 else memory[src_1]
        op2 = src_2 if im2 else memory[src_2]
        memory[dst_addr] = 1 if op1 == op2 else 0
        return ip + 4

    instr = str(memory[ip])
    if instr == '99':
        #print(f'{id} : Killed')
        return -1

    instr = instr.zfill(4)

    if instr[3] == '1': # add
        ip = op_add(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
    elif instr[3] == '2': # mul
        ip = op_mul(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
    elif instr[3] == '3': # store input
        # input available?
        if len(inputs) > 0:
            ip = op_input(memory[ip+1], inputs.pop(0), ip)
        else:
            #print(f'{id} : Waiting for input')
            pass
    elif instr[3] == '4': # output
        ip, last_output = op_output(memory[ip+1], instr[1] == '1', ip)
        #print(f'{id} output : {last_output}')
        outputs.append(last_output)
    elif instr[3] == '5': # jt
        ip = op_jt(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', ip)
    elif instr[3] == '6': # jf
        ip = op_jf(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', ip)
    elif instr[3] == '7': # lt
        ip = op_lt(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
    elif instr[3] == '8': # eq
        ip = op_eq(memory[ip+1], memory[ip+2], instr[1] == '1', instr[0] == '1', memory[ip+3], ip)
    else:
        print(f'{id} wtf')

    return ip


def main_b():

    r = 0
    for phase in permutations(range(5, 10)):

        with open('day07_input.txt', 'r') as f:
            memory = list(map(int, f.read().split(',')))

        inputs_a = [phase[0], 0]
        outputs_a = [phase[1]]
        outputs_b = [phase[2]]
        outputs_c = [phase[3]]
        outputs_d = [phase[4]]

        programs = [[list(memory), 0] for _ in range(5)] # code, ip, inputs, outputs
        programs[0].append(inputs_a)
        programs[0].append(outputs_a)

        programs[1].append(outputs_a)
        programs[1].append(outputs_b)

        programs[2].append(outputs_b)
        programs[2].append(outputs_c)

        programs[3].append(outputs_c)
        programs[3].append(outputs_d)

        programs[4].append(outputs_d)
        programs[4].append(inputs_a)

        while True:
            for i in range(5):
                if programs[i][1] == -1:
                    continue

                programs[i][1] = step_program(i, *programs[i])
            if programs[4][1] == -1:
                r = max(inputs_a[0], r)
                break

    print(f'B: {r}')

if __name__ == '__main__':
    main_a()
    main_b()