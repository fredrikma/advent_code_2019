from textwrap import wrap

def main():
    with open('day08_input.txt', 'r') as f:
        image = f.read()

    data = wrap(image, 25*6)
    c = sorted(list(map(lambda x: (x.count('0'), x.count('1'), x.count('2')), data)), key=lambda x: x[0])
    print(f'A: {c[0][1] * c[0][2]}')

    r = [None] * 25 * 6
    for y in range(6):
        for x in range(25):
            for l in range(len(data)):
                if data[l][y*25+x] != '2':
                    r[y*25+x] = data[l][y*25+x]
                    break

    data = wrap(''.join(r).replace('0', ' '), 25)
    for d in data:
        print(d)

if __name__ == '__main__':
    main()