from textwrap import wrap

memory = []
ip = 0
rel_addr = 0

def run_program_to_output(input: int):
    global ip, rel_addr, memory

    def read(addr, mode):
        if mode == '0': # ordinary
            return memory[addr]
        if mode == '1': # immediate
            return addr
        if mode == '2': # relative
            return memory[addr + rel_addr]
        print('wtf')
    
    def write(addr, value, mode='0'):
        if mode == '0': # ordinary
            memory[addr] = value
        elif mode == '2': # relative
            memory[addr + rel_addr] = value
        else:
            print('wtf')


    def op_add(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 + op2, m_d)
        return ip + 4

    def op_mul(src_1, src_2, m_1, m_2, dst_addr, m_d):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, op1 * op2, m_d)
        return ip + 4

    def op_input(dst_addr, m_d, input):
        write(dst_addr, input, m_d)
        return ip + 2

    def op_output(src_addr, im):
        op1 = read(src_addr, im)
        #print(op1)
        return ip + 2, op1

    def op_jt(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 == 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_jf(src_1, src_2, m_1, m_2):
        op1 = read(src_1, m_1)
        if op1 != 0:
            return ip + 3

        op2 = read(src_2, m_2)
        return op2

    def op_lt(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 < op2 else 0, m_3)
        return ip + 4

    def op_eq(src_1, src_2, m_1, m_2, dst_addr, m_3):
        op1 = read(src_1, m_1)
        op2 = read(src_2, m_2)
        write(dst_addr, 1 if op1 == op2 else 0, m_3)
        return ip + 4

    def op_adjr(src_1, m_1):
        global rel_addr
        op1 = read(src_1, m_1)
        rel_addr += op1
        return ip + 2

    OFFSET_OPCODE = 4
    OFFSET_M_1 = 2
    OFFSET_M_2 = 1
    OFFSET_M_3 = 0

    while True:
        instr = str(memory[ip])
        if instr == '99':
            return -1

        instr = instr.zfill(5)
        #print(instr)

        if instr[OFFSET_OPCODE] == '1': # add
            ip = op_add(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '2': # mul
            ip = op_mul(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '3': # store input
            ip = op_input(memory[ip+1], instr[OFFSET_M_1], input)
        elif instr[OFFSET_OPCODE] == '4': # output
            ip, output = op_output(memory[ip+1], instr[OFFSET_M_1])
            return output
        elif instr[OFFSET_OPCODE] == '5': # jt
            ip = op_jt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '6': # jf
            ip = op_jf(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2])
        elif instr[OFFSET_OPCODE] == '7': # lt
            ip = op_lt(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '8': # eq
            ip = op_eq(memory[ip+1], memory[ip+2], instr[OFFSET_M_1], instr[OFFSET_M_2], memory[ip+3], instr[OFFSET_M_3])
        elif instr[OFFSET_OPCODE] == '9': # relative addr
            ip = op_adjr(memory[ip+1], instr[OFFSET_M_1])
        else:
            print('wtf')



def paint_hull(start_color):
    global memory, ip, rel_addr
    ip = 0
    rel_addr = 0

    with open('day11_input.txt', 'r') as f:
        memory = list(map(lambda x: int(x), f.read().split(',')))

    # extend memory
    memory += [0]*10000

    curr_pos = (0, 0) # moves in positive area
    offsets = {'U': (0, -1), 'D': (0, 1), 'L': (-1, 0), 'R': (1, 0)}

    dirs = 'URDL'
    curr_dir = 0

    hull = {}
    hull[curr_pos] = [start_color]
    while True:
        curr_color = hull.get(curr_pos, [0])[-1]
        #print(f'current color : {curr_color}')
        color = run_program_to_output(curr_color)
        #print(ip)
        if color == -1:
            break

        if curr_pos not in hull:
            hull[curr_pos] = []

        hull[curr_pos].append(color)

        turn = run_program_to_output(None)
        if turn == 0:
            curr_dir -= 1
        elif turn == 1:
            curr_dir += 1
        else:
            break
        
        curr_dir %= 4

        #print(dirs[curr_dir])
        d = offsets[dirs[curr_dir]]
        curr_pos = (curr_pos[0] + d[0], curr_pos[1] + d[1])
        #print(curr_pos)

    return hull


    #run_program(2)

def main():
    #hull = paint_hull(0)
    #print(f'A: {len(hull.keys())}')

    hull = paint_hull(1)
    print(f'B: {len(hull.keys())}')

    size_x = max(hull.keys(), key=lambda x: x[0])[0]+1
    size_y = max(hull.keys(), key=lambda x: x[1])[1]+1

    image = ['.'] * size_x * size_y
    for y in range(size_y):
        for x in range(size_x):
            if (x, y) in hull:
                image[y * size_x + x] = '#' if hull[(x, y)][-1] else '.'

    image = wrap(''.join(image), size_x)
    for l in image:
        print(l)




    
if __name__ == '__main__':
    main()


    
